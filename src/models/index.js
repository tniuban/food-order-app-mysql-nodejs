require('colors');
const { Sequelize } = require('sequelize');
const config = require('../config');

const sequelize = new Sequelize(
  config.dbname,
  config.username,
  config.password,
  {
    dialect: 'mysql',
    host: config.host,
    port: config.port,
  }
);

(async () => {
  try {
    await sequelize.authenticate();
    console.log('Sequelize Connected'.green);
  } catch (error) {
    console.log('Sequelize Error'.red, error);
  }
})();
const User = require('./User')(sequelize),
  Restaurant = require('./Restaurant')(sequelize),
  RestaurantLike = require('./RestaurantLike')(sequelize),
  RestaurantRate = require('./RestaurantRate')(sequelize),
  Order = require('./Order')(sequelize),
  FoodType = require('./FoodType')(sequelize),
  SubFood = require('./SubFood')(sequelize),
  Food = require('./Food')(sequelize);

// user - restaurant
Restaurant.belongsTo(User, { foreignKey: 'ownerId', as: 'owner' });
User.hasMany(Restaurant, { foreignKey: 'ownerId', as: 'restaurants' });

// user - restaurant through restaurant-likes
User.belongsToMany(Restaurant, {
  as: 'restaurantLikes',
  through: RestaurantLike,
  foreignKey: 'userId',
});
Restaurant.belongsToMany(User, {
  as: 'userLikes',
  through: RestaurantLike,
  foreignKey: 'restaurantId',
});

// user - restaurant through restaurant-rates
User.belongsToMany(Restaurant, {
  as: 'restaurantRates',
  through: RestaurantRate,
  foreignKey: 'userId',
});
Restaurant.belongsToMany(User, {
  as: 'userRates',
  through: RestaurantRate,
  foreignKey: 'restaurantId',
});

// food - food_type
Food.belongsTo(FoodType, { foreignKey: 'typeId', as: 'type' });
FoodType.hasMany(Food, { foreignKey: 'typeId', as: 'foods' });

// food - sub_food
SubFood.belongsTo(Food, { foreignKey: 'foodId', as: 'food' });
Food.hasMany(SubFood, { foreignKey: 'foodId', as: 'subFoods' });

// order - user
Order.belongsTo(User, { foreignKey: 'userId', as: 'user' });
User.hasMany(Order, { foreignKey: 'userId', as: 'order' });

// order - food
Order.belongsTo(Food, { foreignKey: 'foodId', as: 'food' });
Food.hasMany(Order, { foreignKey: 'foodId', as: 'order' });

module.exports = {
  sequelize,
  User,
  Restaurant,
  RestaurantLike,
  RestaurantRate,
  Order,
  FoodType,
  Food,
  SubFood,
};
