const { DataTypes } = require('sequelize');

module.exports = (sequelize) =>
  sequelize.define(
    'SubFood',
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      name: {
        type: DataTypes.STRING,
      },
      price: {
        type: DataTypes.INTEGER,
      },
      foodId: {
        field: 'food_id',
        type: DataTypes.INTEGER,
      },
    },
    {
      tableName: 'sub_foods',
      timestamps: false,
    }
  );
