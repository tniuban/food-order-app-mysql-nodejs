const { DataTypes } = require('sequelize');
const bcrypt = require('bcrypt');

module.exports = (sequelize) =>
  sequelize.define(
    'User',
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      firstName: {
        type: DataTypes.STRING,
        field: 'first_name',
        // get() {
        //   return this.dataValues.firstName;
        // },
      },
      lastName: {
        type: DataTypes.STRING,
        field: 'last_name',
      },
      email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
        validate: {
          isEmail: {
            msg: 'Invalid email',
          },
        },
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false,
        set(value) {
          // password không được plaintext xuống db > hash
          const hashPw = bcrypt.hashSync(
            value,
            '$2b$10$zVFdKuJoPHwiwsHe.HGOCO'
          );
          this.setDataValue('password', hashPw);
        },
        // validate: {
        //   isMatchedConfirmPassword(value) {
        //     if (value !== this.confirmPassrod) {
        //       throw new Error('Password not matched');
        //     }
        //   },
        //   minLength(value) {
        //     if (value.length < 8) {
        //       throw new Error('Password is longer than 8 characters.');
        //     }
        //   },
        // },
      },
    },
    {
      tableName: 'users',
      // Remove field when find
      defaultScope: {
        attributes: {
          exclude: 'password',
        },
      },
      hooks: {
        afterSave: (record) => {
          delete record.dataValues.password;
        },
      },
      timestamps: false,
      createdAt: false,
      updatedAt: false,
    }
  );
