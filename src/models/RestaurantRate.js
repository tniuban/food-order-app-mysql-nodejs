const { DataTypes, Sequelize } = require('sequelize');

module.exports = (sequelize) =>
  sequelize.define(
    'RestaurantRate',
    {
      userId: {
        type: DataTypes.INTEGER,
        field: 'user_id',
      },
      restaurantId: {
        type: DataTypes.INTEGER,
        field: 'restaurant_id',
      },
      amount: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      date_rate: {
        type: DataTypes.DATE,
        field: 'date_rate',
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
    },
    {
      tableName: 'restaurant-rates',
      timestamps: false,
    }
  );
