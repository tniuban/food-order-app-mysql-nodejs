const { DataTypes } = require('sequelize');

module.exports = (sequelize) => {
  return sequelize.define(
    'Restaurant',
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      name: {
        type: DataTypes.STRING(50),
        allowNull: false,
      },
      description: {
        type: DataTypes.STRING,
      },
      ownerId: {
        type: DataTypes.INTEGER,
        field: 'owner_id',
        allowNull: false,
      },
    },
    {
      tableName: 'restaurants',
      timestamps: false,
    }
  );
};
