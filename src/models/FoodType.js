const { DataTypes } = require('sequelize');

module.exports = (sequelize) =>
  sequelize.define(
    'FoodType',
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      name: {
        type: DataTypes.STRING(255),
      },
    },
    {
      tableName: 'food_types',
      timestamps: false,
    }
  );
