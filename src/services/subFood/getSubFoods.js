const { SugarError } = require('../../helpers/errors');
const { SubFood } = require('../../models');

module.exports = async ({ limit, offset }) => {
  try {
    const subFoods = await SubFood.findAll({
      limit: limit + 1,
      offset,
    });

    let nextPagination = null;

    if (subFoods.length > limit) {
      nextPagination = { limit, offset: offset + limit };
      subFoods.pop();
    }

    return { subFoods, nextPagination };
  } catch (error) {
    throw new SugarError(error);
  }
};
