const { SugarError } = require('../../helpers/errors');
const { SubFood } = require('../../models');

module.exports = async ({ name, price, foodId }) => {
  try {
    const subFood = await SubFood.create({ name, price, foodId });
    return subFood;
  } catch (error) {
    throw new SugarError(error);
  }
};
