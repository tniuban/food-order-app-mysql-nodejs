const { SugarError } = require('../../helpers/errors');
const { SubFood } = require('../../models');

module.exports = async (id) => {
	try {
		const subFood = await SubFood.findOne({ where: { id } });
		return subFood;
	} catch (error) {
		throw new SugarError(error);
	}
};
