const { SugarError } = require('../../helpers/errors');
const { SubFood } = require('../../models');

module.exports = async (id) => {
	try {
		await SubFood.destroy({ where: { id } });
		return true;
	} catch (error) {
		throw new SugarError(error);
	}
};
