const { SugarError } = require('../../helpers/errors');
const { SubFood } = require('../../models');

module.exports = async ({ id, name, price, foodId }) => {
  try {
    const subFood = await SubFood.update(
      { name, price, foodId },
      { where: { id } }
    );
    return subFood;
  } catch (error) {
    throw new SugarError(error);
  }
};
