const { SugarError } = require('../../helpers/errors');
const { FoodType } = require('../../models');

module.exports = async ({ id, name }) => {
  try {
    const foodType = await FoodType.update({ name }, { where: { id } });
    return foodType;
  } catch (error) {
    throw new SugarError(error);
  }
};
