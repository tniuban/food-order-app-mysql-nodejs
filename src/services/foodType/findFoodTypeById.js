const { SugarError } = require('../../helpers/errors');
const { FoodType } = require('../../models');

module.exports = async (id) => {
	try {
		const foodType = await FoodType.findOne({ where: { id } });
		return foodType;
	} catch (error) {
		throw new SugarError(error);
	}
};
