const { SugarError } = require('../../helpers/errors');
const { FoodType } = require('../../models');

module.exports = async ({ limit, offset }) => {
  try {
    const foodTypes = await FoodType.findAll({
      include: [
        {
          association: 'foods',
          attributes: {
            exclude: ['typeId'],
          },
        },
      ],
      limit: limit + 1,
      offset,
    });

    let nextPagination = null;

    if (foodTypes.length > limit) {
      nextPagination = { limit, offset: offset + limit };
      foodTypes.pop();
    }

    return { foodTypes, nextPagination };
  } catch (error) {
    throw new SugarError(error);
  }
};
