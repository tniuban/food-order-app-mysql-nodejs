const { SugarError } = require('../../helpers/errors');
const { FoodType } = require('../../models');

module.exports = async ({ name }) => {
  try {
    const foodType = await FoodType.create({ name });
    return foodType;
  } catch (error) {
    throw new SugarError(error);
  }
};
