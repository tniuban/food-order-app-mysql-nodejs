const { SugarError } = require('../../helpers/errors');
const { FoodType } = require('../../models');

module.exports = async (id) => {
	try {
		await FoodType.destroy({ where: { id } });
		return true;
	} catch (error) {
		throw new SugarError(error);
	}
};
