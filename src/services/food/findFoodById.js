const { SugarError } = require('../../helpers/errors');
const { Food } = require('../../models');

module.exports = async (id) => {
	try {
		const food = await Food.findOne({ where: { id } });
		return food;
	} catch (error) {
		throw new SugarError(error);;
	}
};
