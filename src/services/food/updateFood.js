const { SugarError } = require('../../helpers/errors');
const { Food } = require('../../models');

module.exports = async ({ id, name, image, price, desc, typeId }) => {
  try {
    const food = await Food.update(
      { name, image, price, desc, typeId },
      { where: { id } }
    );
    return food;
  } catch (error) {
    throw new SugarError(error);
  }
};
