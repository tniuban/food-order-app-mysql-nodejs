const { SugarError } = require('../../helpers/errors');
const { Food } = require('../../models');

module.exports = async ({ name, image, price, desc, typeId }) => {
  try {
    const food = await Food.create({
      name,
      image,
      price,
      desc,
      typeId,
    });
    return food;
  } catch (error) {
    throw new SugarError(error);
  }
};
