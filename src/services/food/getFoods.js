const { SugarError } = require('../../helpers/errors');
const { Food } = require('../../models');

module.exports = async ({ limit, offset }) => {
  try {
    const foods = await Food.findAll({
      // attributes: {
      //   exclude: ['typeId'],
      // },
      include: [
        // 'type',
        {
          association: 'subFoods',
          attributes: {
            exclude: ['foodId'],
          },
        },
      ],
      limit: limit + 1,
      offset,
    });

    let nextPagination = null;

    if (foods.length > limit) {
      nextPagination = { limit, offset: offset + limit };
      foods.pop();
    }

    return { foods, nextPagination };
  } catch (error) {
    throw new SugarError(error);
  }
};
