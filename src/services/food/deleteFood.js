const { SugarError } = require('../../helpers/errors');
const { Food } = require('../../models');

module.exports = async (id) => {
	try {
		await Food.destroy({ where: { id } });
		return true;
	} catch (error) {
		throw new SugarError(error);;
	}
};
