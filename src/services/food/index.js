const foodServices = {
	createFood: require('./createFood'),
	getFoods: require('./getFoods'),
	findFoodById: require('./findFoodById'),
	updateFood: require('./updateFood'),
	deleteFood: require('./deleteFood'),
};

module.exports = foodServices;
