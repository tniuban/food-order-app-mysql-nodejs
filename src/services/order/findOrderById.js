const { SugarError } = require('../../helpers/errors');
const { Order } = require('../../models');

module.exports = async (id) => {
	try {
		const order = await Order.findOne({ where: { id } });
		return order;
	} catch (error) {
		throw new SugarError(error);;
	}
};
