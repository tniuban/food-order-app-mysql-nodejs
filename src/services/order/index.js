const orderServices = {
	createOrder: require('./createOrder'),
	getOrders: require('./getOrders'),
	findOrderById: require('./findOrderById'),
	updateOrder: require('./updateOrder'),
	deleteOrder: require('./deleteOrder'),
};

module.exports = orderServices;
