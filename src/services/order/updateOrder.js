
const { SugarError } = require('../../helpers/errors');
const { Order } = require('../../models');

module.exports = async ({ id, ...args }) => {
	try {
		const order = await Order.update({ ...args }, { where: { id } });
		return order;
	} catch (error) {
		throw new SugarError(error);;
	}
};
