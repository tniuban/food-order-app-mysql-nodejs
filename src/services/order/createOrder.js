const { SugarError } = require('../../helpers/errors');
const { Order } = require('../../models');

module.exports = async ({ userId, foodId, amount, code, arrSubId }) => {
  try {
    const order = await Order.create({
      userId,
      foodId,
      amount,
      code,
      arrSubId,
    });
    return order;
  } catch (error) {
    throw new SugarError(error);
  }
};
