const { SugarError } = require('../../helpers/errors');
const { Order } = require('../../models');

module.exports = async (id) => {
	try {
		await Order.destroy({ where: { id } });
		return true;
	} catch (error) {
		throw new SugarError(error);;
	}
};
