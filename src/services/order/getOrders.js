const { SugarError } = require('../../helpers/errors');
const { Order } = require('../../models');

module.exports = async ({ limit, offset }) => {
  try {
    const orders = await Order.findAll({
      include: ['food', 'user'],
      limit: limit + 1,
      offset,
    });

    let nextPagination = null;

    if (orders.length > limit) {
      nextPagination = { limit, offset: offset + limit };
      orders.pop();
    }

    return { orders, nextPagination };
  } catch (error) {
    throw new SugarError(error);
  }
};
