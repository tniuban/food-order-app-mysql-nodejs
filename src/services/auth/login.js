const { SugarError } = require('../../helpers/errors');
const { User } = require('../../models');
const bcrypt = require('bcrypt');
const { ErrWrongEmail } = require('../../pkg/appError');
const { Sequelize } = require('sequelize');

module.exports = async ({ email, password }) => {
  try {
    const user = await User.findOne({
      where: { email },
      include: {
        association: 'restaurants',
        attributes: { exclude: 'ownerId' },
      },
      attributes: {
        include: ['password'],
      },
    });

    if (!user) {
      throw new SugarError(ErrWrongEmail);
    }

    const hashedPassword = bcrypt.compare(password, user.password);
    if (!hashedPassword) {
      throw new SugarError(ErrWrongEmail);
    }
    delete user.dataValues.password;
    return user;
  } catch (error) {
    throw new SugarError(error);
  }
};
