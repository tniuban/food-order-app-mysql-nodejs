
const { SugarError } = require('../../helpers/errors');
const { User } = require('../../models');

module.exports = async ({ id, ...args }) => {
	try {
		const user = await User.update({ ...args }, { where: { id } });
		return user;
	} catch (error) {
		throw new SugarError(error);;
	}
};
