
const { SugarError } = require('../../helpers/errors');
const { User } = require('../../models');

module.exports = async (args) => {
	try {
		const user = await User.create(args);
		return user;
	} catch (error) {
		throw new SugarError(error);;
	}
};
