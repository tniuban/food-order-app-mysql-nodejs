
const { SugarError } = require('../../helpers/errors');
const { User } = require('../../models');

module.exports = async (id) => {
	try {
		await User.destroy({ where: { id } });
		return true;
	} catch (error) {
		throw new SugarError(error);;
	}
};
