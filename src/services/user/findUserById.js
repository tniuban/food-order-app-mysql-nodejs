
const { SugarError } = require('../../helpers/errors');
const { User } = require('../../models');

module.exports = async (id) => {
	try {
		const user = await User.findOne({ where: { id } });
		return user;
	} catch (error) {
		throw new SugarError(error);;
	}
};
