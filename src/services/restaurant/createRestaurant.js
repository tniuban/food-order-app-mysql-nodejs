const { SugarError } = require('../../helpers/errors');
const { Restaurant } = require('../../models');

module.exports = async ({ ownerId, name, description }) => {
  try {
    const restaurant = await Restaurant.create({ ownerId, name, description });
    return restaurant;
  } catch (error) {
    throw new SugarError(error);
  }
};
