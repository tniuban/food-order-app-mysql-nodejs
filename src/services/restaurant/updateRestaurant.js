const { SugarError } = require('../../helpers/errors');
const { Restaurant } = require('../../models');

module.exports = async ({ id, ownerId, name, description }) => {
  try {
    const restaurant = await Restaurant.update(
      { ownerId, name, description },
      { where: { id } }
    );
    return restaurant;
  } catch (error) {
    throw SugarError(error);
  }
};
