const { SugarError } = require('../../helpers/errors');
const { Restaurant } = require('../../models');

module.exports = async ({ userId, restaurantId }) => {
  try {
    const restaurant = await Restaurant.findByPk(restaurantId);
    if (!restaurant) {
      throw new SugarError({
        code: 2000,
        message: 'Restaurant not found',
      });
    }

    const hasLiked = await restaurant.hasUserLike(userId);

    if (hasLiked) {
      await restaurant.removeUserLike(userId);
    } else {
      await restaurant.addUserLike(userId);
    }

    return {
      restaurantId,
      userId,
    };
  } catch (error) {
    throw new SugarError(error);
  }
};
