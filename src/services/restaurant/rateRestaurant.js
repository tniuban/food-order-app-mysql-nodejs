const { SugarError } = require('../../helpers/errors');
const { Restaurant } = require('../../models');

module.exports = async ({ userId, restaurantId, amount }) => {
  try {
    const restaurant = await Restaurant.findByPk(restaurantId);
    if (!restaurant) {
      throw new SugarError({
        code: 2000,
        message: 'Restaurant not found',
      });
    }

    await restaurant.addUserRate(userId, { through: { amount } });
    return {
      restaurantId,
      userId,
      amount,
    };
  } catch (error) {
    throw new SugarError(error);
  }
};
