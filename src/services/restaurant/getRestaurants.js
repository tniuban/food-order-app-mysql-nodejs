const { SugarError } = require('../../helpers/errors');
const { Restaurant } = require('../../models');

module.exports = async ({ limit = 5, offset = 0 }) => {
  try {
    const restaurants = await Restaurant.findAll({
      attributes: { exclude: ['ownerId'] },
      include: [
        'owner',
        {
          association: 'userLikes',
          attributes: ['id'],
          through: {
            attributes: [],
          },
        },
      ],
      limit: limit + 1,
      offset,
    });

    let nextPagination = null;

    if (restaurants.length > limit) {
      nextPagination = { limit, offset: offset + limit };
      restaurants.pop();
    }

    return { restaurants, nextPagination };
  } catch (error) {
    throw new SugarError(error);
  }
};
