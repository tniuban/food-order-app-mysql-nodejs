const { SugarError } = require('../../helpers/errors');
const { Restaurant } = require('../../models');

module.exports = async (id) => {
  try {
    const restaurant = await Restaurant.findByPk(id);
    if (!restaurant) {
      throw new SugarError({
        code: 1005,
        message: 'Restaurant not found',
      });
    }
    return restaurant;
  } catch (error) {
    throw new SugarError(error);
  }
};
