const restaurantServices = {
  createRestaurant: require('./createRestaurant'),
  getRestaurants: require('./getRestaurants'),
  findRestaurantById: require('./findRestaurantById'),
  updateRestaurant: require('./updateRestaurant'),
  deleteRestaurant: require('./deleteRestaurant'),
  likeRestaurant: require('./likeRestaurant'),
  getRestaurantLikes: require('./getRestaurantLikes'),
  rateRestaurant: require('./rateRestaurant'),
  getRestaurantRates: require('./getRestaurantRates'),
};

module.exports = restaurantServices;
