const { SugarError } = require('../../helpers/errors');
const { RestaurantLike } = require('../../models');
const R = require('ramda');

module.exports = async ({ userId, restaurantId, limit = 5, offset = 0 }) => {
  const filter = R.reject(R.isNil, { userId, restaurantId });
  try {
    const restaurantLikes = await RestaurantLike.findAll({
      where: filter,
      limit: limit + 1,
      offset,
    });

    let nextPagination = null;

    if (restaurantLikes.length > limit) {
      nextPagination = { limit, offset: offset + limit };
      restaurantLikes.pop();
    }

    return { restaurantLikes, nextPagination };
  } catch (error) {
    throw new SugarError(error);
  }
};
