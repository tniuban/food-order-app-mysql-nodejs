
const { SugarError } = require('../../helpers/errors');
const { Restaurant } = require('../../models');

module.exports = async (id) => {
	try {
		await Restaurant.destroy({ where: { id } });
		return true;
	} catch (error) {
		throw new SugarError(error);;
	}
};
