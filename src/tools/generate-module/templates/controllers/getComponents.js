module.exports = (componentName, modelName) => ({
  content: `const resp = require('../../helpers/response');
const ${componentName}Services = require('../../services/${componentName}');

module.exports = async (req, res, next) => {
	const args = req.body;
	try {
		const ${componentName} = await ${componentName}Services.get${modelName}s(args);
		resp({
			res,
			data: ${componentName},
		});
	} catch (error) {
		next(error);
	}
};
`,
  name: `get${modelName}s.js`,
});
