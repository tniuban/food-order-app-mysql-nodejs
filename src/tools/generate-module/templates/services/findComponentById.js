module.exports = (componentName, modelName) => ({
  content: `const { SugarError } = require('../../helpers/errors');
const { ${modelName} } = require('../../models');

module.exports = async (id) => {
	try {
		const ${componentName} = await ${modelName}.findOne({ where: { id } });
		return ${componentName};
	} catch (error) {
		throw new SugarError(error);
	}
};
`,
  name: `find${modelName}ById.js`,
});
