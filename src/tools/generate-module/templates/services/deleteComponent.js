module.exports = (_componentName, modelName) => ({
  content: `const { SugarError } = require('../../helpers/errors');
const { ${modelName} } = require('../../models');

module.exports = async (id) => {
	try {
		await ${modelName}.destroy({ where: { id } });
		return true;
	} catch (error) {
		throw new SugarError(error);
	}
};
`,
  name: `delete${modelName}.js`,
});
