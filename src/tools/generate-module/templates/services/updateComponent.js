module.exports = (componentName, modelName) => ({
  content: `
const { SugarError } = require('../../helpers/errors');
const { ${modelName} } = require('../../models');

module.exports = async ({ id, ...args }) => {
	try {
		const ${componentName} = await ${modelName}.update({ ...args }, { where: { id } });
		return ${componentName};
	} catch (error) {
		throw new SugarError(error);
	}
};
`,
  name: `update${modelName}.js`,
});
