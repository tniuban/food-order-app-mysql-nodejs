const componentControllers = require('./controllers');
const componentServices = require('./services');
const componentModel = require('./model');

module.exports = { componentControllers, componentServices, componentModel };
