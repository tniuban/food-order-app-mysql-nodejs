require('colors');
const fs = require('fs');
const {
  componentControllers,
  componentServices,
  componentModel,
} = require('./templates');

const params = process.argv.slice(2);
params.forEach((param) => {
  if (!param) {
    console.error('Please supply a valid component name'.red);
    process.exit(1);
  }

  const tableName = param;
  const modelName = tableName
    .split('_')
    .map((name) => name[0].toUpperCase() + name.slice(1))
    .join('');
  const componentName = tableName[0] + modelName.slice(1);

  console.log('tableName', tableName);
  console.log('modelName', modelName);
  console.log('componentName', componentName);

  console.log(`Creating Component Templates with name: ${componentName}`);

  const controllersDirectory = `./src/controllers/${componentName}`;
  const modelDirectiory = `./src/models`;
  const servicesDirectory = `./src/services/${componentName}`;

  /** Model genration */
  const generateModelTemplates = componentModel(
    componentName,
    modelName,
    tableName
  );
  fs.writeFileSync(
    `${modelDirectiory}/${generateModelTemplates.name}`,
    generateModelTemplates.content
  );

  /** Controllers genration */
  if (fs.existsSync(controllersDirectory)) {
    console.error(`Component ${componentName} already exists.`.red);
    process.exit(1);
  }

  fs.mkdirSync(controllersDirectory);
  const generateControllersTemplates = componentControllers.map((template) =>
    template(componentName, modelName)
  );

  generateControllersTemplates.forEach((template) => {
    fs.writeFileSync(
      `${controllersDirectory}/${template.name}`,
      template.content
    );
  });

  /** Services genration */
  if (fs.existsSync(servicesDirectory)) {
    console.error(`Component ${componentName} already exists.`.red);
    process.exit(1);
  }

  fs.mkdirSync(servicesDirectory);
  const generateServicesTemplates = componentServices.map((template) =>
    template(componentName, modelName)
  );

  generateServicesTemplates.forEach((template) => {
    fs.writeFileSync(`${servicesDirectory}/${template.name}`, template.content);
  });

  console.log(
    `Successfully created model component under: ${modelDirectiory.green}`
  );
  console.log(
    `Successfully created controllers component under: ${servicesDirectory.green}`
  );
  console.log(
    `Successfully created services component under: ${servicesDirectory.green}`
  );
});
