//DO NOT EDIT: code generated from 'tools/gen-err-code.go'

const { SugarError } = require('../helpers/errors');

const errNameToAppError = {
  ErrUnknown: new SugarError({
    statusCode: 200,
    code: 1000,
    message: 'Something went wrong',
  }),
  ErrInvalidArgument: new SugarError({
    statusCode: 200,
    code: 1001,
    message: 'Invalid argument',
  }),
  ErrWrongEmail: new SugarError({
    statusCode: 200,
    code: 1002,
    message: 'Wrong email',
  }),
  ErrWrongPassword: new SugarError({
    statusCode: 200,
    code: 1003,
    message: 'Wrong password',
  }),
  ErrAccountInActive: new SugarError({
    statusCode: 200,
    code: 1004,
    message: 'Your account is inactive',
  }),
  ErrUserNotFound: new SugarError({
    statusCode: 200,
    code: 1005,
    message: 'User not found',
  }),
  ErrPermissionDenied: new SugarError({
    statusCode: 200,
    code: 1006,
    message: 'Permission denied',
  }),
  ErrAccountExisted: new SugarError({
    statusCode: 200,
    code: 1007,
    message: 'Account already existed',
  }),
  ErrInvalidOldPassword: new SugarError({
    statusCode: 200,
    code: 1008,
    message: 'Old password does not match',
  }),
  ErrInvalidOTP: new SugarError({
    statusCode: 200,
    code: 1009,
    message: 'Invalid OTP',
  }),
  ErrAzureDeviceOffline: new SugarError({
    statusCode: 200,
    code: 1010,
    message: 'Azure device is offline',
  }),
  ErrAzureDeviceDeviceNotFound: new SugarError({
    statusCode: 200,
    code: 1011,
    message: 'Azure device not found',
  }),
  ErrCanNotEditFarmImportWarehouse: new SugarError({
    statusCode: 200,
    code: 1012,
    message: "Only import status 'DRAFT' can edit",
  }),
  ErrCanNotDeleteFarmImportWarehouse: new SugarError({
    statusCode: 200,
    code: 1013,
    message: "Only import status 'DRAFT' can delete",
  }),
  ErrFarmImportWarehouseAlreadyApproved: new SugarError({
    statusCode: 200,
    code: 1014,
    message: 'Farm import already approved',
  }),
  ErrCanNotEditModuleTransferWarehouse: new SugarError({
    statusCode: 200,
    code: 1015,
    message: "Only transfer status 'DRAFT' can edit",
  }),
  ErrCanNotDeleteModuleTransferWarehouse: new SugarError({
    statusCode: 200,
    code: 1016,
    message: "Only transfer status 'DRAFT' can delete",
  }),
  ErrModuleTransferWarehouseAlreadyApproved: new SugarError({
    statusCode: 200,
    code: 1017,
    message: 'Module transfer already approved',
  }),
  ErrShrimpDiaryNotFound: new SugarError({
    statusCode: 200,
    code: 1018,
    message: 'Shrimp Diary Not Found',
  }),
  ErrNotEnoughtProductQuantityToUse: new SugarError({
    statusCode: 200,
    code: 1019,
    message: 'Not Enought Product Quantity',
  }),
  ErrNotFoundModuleWarehouse: new SugarError({
    statusCode: 200,
    code: 1020,
    message: 'Not Found Module Warehouse',
  }),
  ErrPondCodeExisted: new SugarError({
    statusCode: 200,
    code: 1021,
    message: 'Pond code Existed',
  }),
  ErrUserHasExistedRole: new SugarError({
    statusCode: 200,
    code: 1022,
    message: 'User has existed role',
  }),
  ErrInvalidDateRange: new SugarError({
    statusCode: 200,
    code: 1023,
    message: 'Invalid date range',
  }),
  ErrNotFoundReportPerformance: new SugarError({
    statusCode: 200,
    code: 1024,
    message: 'Report performance not found',
  }),
  ErrNotFoundActiveSeason: new SugarError({
    statusCode: 200,
    code: 1025,
    message: 'Not found active season',
  }),
  ErrIotPondNotFound: new SugarError({
    statusCode: 200,
    code: 1026,
    message: 'Iot pond not found',
  }),
  ErrIotPondAssignExisted: new SugarError({
    statusCode: 200,
    code: 1027,
    message: 'Iot pond assign existed',
  }),
  ErrPondAssignExisted: new SugarError({
    statusCode: 200,
    code: 1028,
    message: 'Pond assign existed',
  }),
  ErrTaskCanNotModify: new SugarError({
    statusCode: 200,
    code: 1029,
    message: 'Task can not modify',
  }),
  ErrEdgeDeviceNotFound: new SugarError({
    statusCode: 200,
    code: 1030,
    message: 'Edge device not found',
  }),
  ErrImportFailed: new SugarError({
    statusCode: 200,
    code: 1031,
    message: 'Import failed',
  }),
  ErrFarmCodeExisted: new SugarError({
    statusCode: 200,
    code: 1032,
    message: 'Farm code Existed',
  }),
  ErrZoneCodeExisted: new SugarError({
    statusCode: 200,
    code: 1033,
    message: 'Zone code Existed',
  }),
  ErrModuleCodeExisted: new SugarError({
    statusCode: 200,
    code: 1034,
    message: 'Zone code Existed',
  }),
  ErrProductCodeExisted: new SugarError({
    statusCode: 200,
    code: 1035,
    message: 'Product code Existed',
  }),
  ErrNotFoundShrimpDiaryActiveOrHarvesting: new SugarError({
    statusCode: 200,
    code: 1036,
    message: 'Not Found Shrimp Diary Active Or Harvesting',
  }),
  ErrProductNotFound: new SugarError({
    statusCode: 200,
    code: 1037,
    message: 'Product not found',
  }),
  ErrMovingToPondNotFinishDiaryYet: new SugarError({
    statusCode: 200,
    code: 1038,
    message: 'Pond not finish diary yet',
  }),
  ErrFarmNotFound: new SugarError({
    statusCode: 200,
    code: 1039,
    message: 'Farm Not Found',
  }),
  ErrProductFarmAlreadyExtis: new SugarError({
    statusCode: 200,
    code: 1040,
    message: 'Product farm already exist',
  }),
  ErrProductFarmNotFound: new SugarError({
    statusCode: 200,
    code: 1041,
    message: 'Product Farm Not Found',
  }),
  ErrUserProductNotFound: new SugarError({
    statusCode: 200,
    code: 1042,
    message: 'User Product Not Found',
  }),
  ErrProductExitsWarehouse: new SugarError({
    statusCode: 200,
    code: 1043,
    message: 'Product Exits Warehouse',
  }),
  ErrUserAlreadyOwnFarm: new SugarError({
    statusCode: 200,
    code: 1044,
    message: 'User Already Own Farm',
  }),
  ErrUserAlreadyOwnZone: new SugarError({
    statusCode: 200,
    code: 1045,
    message: 'User Already Own Zone',
  }),
  ErrUserAlreadyOwnModule: new SugarError({
    statusCode: 200,
    code: 1046,
    message: 'User Already Own Module',
  }),
  ErrUserAlreadyOwnPond: new SugarError({
    statusCode: 200,
    code: 1047,
    message: 'User Already Own Pond',
  }),
  ErrCanNotEditProductCategory: new SugarError({
    statusCode: 200,
    code: 1048,
    message: 'Product already used. Can not edit category',
  }),
  ErrNotShrimpInformationSourcePondYet: new SugarError({
    statusCode: 200,
    code: 1049,
    message: 'Not Shrimp Information Source Pond Yet',
  }),
  ErrTimeZoneInvalid: new SugarError({
    statusCode: 200,
    code: 1050,
    message: 'Timezone in valid',
  }),
  ErrCanNotEditProductUnit: new SugarError({
    statusCode: 200,
    code: 1051,
    message: 'Product already used. Can not edit unit',
  }),
  ErrUserNotOwnFarm: new SugarError({
    statusCode: 200,
    code: 1052,
    message: 'User Not Own Farm',
  }),
  ErrUserNotOwnZone: new SugarError({
    statusCode: 200,
    code: 1053,
    message: 'User Not Own Zone',
  }),
  ErrUserNotOwnModule: new SugarError({
    statusCode: 200,
    code: 1054,
    message: 'User Not Own Module',
  }),
  ErrUserNotOwnPond: new SugarError({
    statusCode: 200,
    code: 1055,
    message: 'User Not Own Pond',
  }),
  ErrNotShrimpInformationTargetPondYet: new SugarError({
    statusCode: 200,
    code: 1056,
    message: 'Not Shrimp Information Target Pond Yet',
  }),
  ErrUnauthenticated: new SugarError({
    statusCode: 401,
    code: 2001,
    message: 'Unauthenticated',
  }),
  ErrAzureSqliteIsCorupted: new SugarError({
    statusCode: 200,
    code: 3001,
    message: 'Could not process command because sqlite is corupted',
  }),
  ErrAzurePondIsNotExisted: new SugarError({
    statusCode: 200,
    code: 3002,
    message: 'Pond is not existed',
  }),
  ErrAzureDateDontMatch: new SugarError({
    statusCode: 200,
    code: 3003,
    message: "Date doesn't match",
  }),
  ErrAzureControlModeIsNotSupported: new SugarError({
    statusCode: 200,
    code: 3004,
    message: 'Control mode is not supported',
  }),
  ErrAzureDeviceNotOke: new SugarError({
    statusCode: 200,
    code: 3005,
    message: 'Device is not OK for controlling',
  }),
  ErrAzureInvalidInpuFormat: new SugarError({
    statusCode: 200,
    code: 3006,
    message: 'Invalid input format',
  }),
  ErrAzureDeviceIsNotExisted: new SugarError({
    statusCode: 200,
    code: 3007,
    message: 'Device is not existed',
  }),
  ErrAzureIotDeviceIsExisted: new SugarError({
    statusCode: 200,
    code: 3008,
    message: 'Device is existed',
  }),
  ErrAzureDeviceTypeInvalid: new SugarError({
    statusCode: 200,
    code: 3009,
    message: 'Type of device is invalid',
  }),
  ErrAzureMotiveBoxIsExisted: new SugarError({
    statusCode: 200,
    code: 3010,
    message: 'MotiveBox is existed',
  }),
  ErrAzurePondExisted: new SugarError({
    statusCode: 200,
    code: 3011,
    message: 'Pond is existed',
  }),
  ErrAzureMotiveBoxNotExisted: new SugarError({
    statusCode: 200,
    code: 3012,
    message: 'MotiveBox is not existed',
  }),
  ErrAzureEmptyData: new SugarError({
    statusCode: 200,
    code: 3013,
    message: 'Cannot update data without parameter',
  }),
  ErrAzurePondTypeInvalid: new SugarError({
    statusCode: 200,
    code: 3014,
    message: 'Type of pond is invalid',
  }),
  ErrAzureTableNotExisted: new SugarError({
    statusCode: 200,
    code: 3015,
    message: 'Table is not existed',
  }),
  ErrAzureFeedingScheduleDataInvalid: new SugarError({
    statusCode: 200,
    code: 3016,
    message: 'Data of feeding schedule is invalid',
  }),
  ErrAzureIotFeedingSpeedInvalid: new SugarError({
    statusCode: 200,
    code: 3017,
    message: 'Data of feeding speed is invalid',
  }),
  ErrAzureModeTypeInvalid: new SugarError({
    statusCode: 200,
    code: 3018,
    message: 'Mode type is invalid',
  }),
  ErrAzureQuantityInvalid: new SugarError({
    statusCode: 200,
    code: 3019,
    message: 'Quantity is invalid',
  }),
  ErrAzureAddressInvalid: new SugarError({
    statusCode: 200,
    code: 3020,
    message: 'Address is invalid',
  }),
  ErrAzureAddressTaken: new SugarError({
    statusCode: 200,
    code: 3021,
    message: 'Address is taken',
  }),
  ErrAzureHarvestingDayNotExisted: new SugarError({
    statusCode: 200,
    code: 3022,
    message: 'Harvesting day is not existed',
  }),
  ErrAzureUnitIDSetup: new SugarError({
    statusCode: 200,
    code: 3023,
    message: 'UnitId setup error',
  }),
  ErrAzureUnitIDExisted: new SugarError({
    statusCode: 200,
    code: 3024,
    message: 'UnitId is existed',
  }),
  ErrAzureFcExisted: new SugarError({
    statusCode: 200,
    code: 3025,
    message: 'Fc is existed',
  }),
  ErrAzureFcIsNotExisted: new SugarError({
    statusCode: 200,
    code: 3026,
    message: 'Fc is not existed',
  }),
  ErrAzureSensorExisted: new SugarError({
    statusCode: 200,
    code: 3027,
    message: 'Sensor is existed',
  }),
  ErrAzureSensorIsNotExisted: new SugarError({
    statusCode: 200,
    code: 3028,
    message: 'Sensor is not existed',
  }),
  ErrAzureImportFaild: new SugarError({
    statusCode: 200,
    code: 3029,
    message: 'Import failed',
  }),
  ErrAzureValueIsDuplicateAndHaveEvent: new SugarError({
    statusCode: 200,
    code: 3030,
    message: 'Value is duplicated and have event',
  }),
  ErrAzureValueIsDuplicateAndDontHaveEvent: new SugarError({
    statusCode: 200,
    code: 3031,
    message: 'Value is duplicated and dont have event',
  }),
  ErrAzureTrackingTypeIsNotExited: new SugarError({
    statusCode: 200,
    code: 3032,
    message: 'Tracking type is not exited',
  }),
  ErrAzureCannotControlFeeder: new SugarError({
    statusCode: 200,
    code: 3033,
    message: 'Cannot control Feeder',
  }),
  ErrAzureTheDrainageHoleIsAlmostEmpty: new SugarError({
    statusCode: 200,
    code: 3034,
    message: 'The drainage hole is almost empty',
  }),
  ErrAzureMissingParams: new SugarError({
    statusCode: 200,
    code: 3401,
    message: 'Missing parameter for this process',
  }),
};

module.exports = errNameToAppError;
