const jwt = require('jsonwebtoken');
const config = require('../config');
const { SugarError } = require('../helpers/errors');
const { ErrUnauthenticated } = require('../pkg/appError');

verifyToken = (req, res, next) => {
  let bearerToken = req.headers['authorization'];
  let token = bearerToken?.split(' ')?.[1];
  jwt.verify(token, config.jwtSecretKey, (error, decoded) => {
    if (error) {
      throw new SugarError(ErrUnauthenticated);
    }
    req.userId = decoded.id;
    req.restaurantIds = decoded.restaurantIds;
    next();
  });
};

module.exports = verifyToken;
