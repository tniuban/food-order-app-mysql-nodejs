const resp = require('../../helpers/response');
const foodServices = require('../../services/food');

module.exports = async (req, res, next) => {
  const { id } = req.params;
  const { name, image, price, desc, typeId } = req.body;
  try {
    const food = await foodServices.updateFood({
      name,
      image,
      price,
      desc,
      typeId,
    });
    resp({
      res,
      data: food,
    });
  } catch (error) {
    next(error);
  }
};
