const resp = require('../../helpers/response');
const foodServices = require('../../services/food');
const { ErrPermissionDenied } = require('../../pkg/appError');
const { SugarError } = require('../../helpers/errors');

module.exports = async (req, res, next) => {
  const { name, image, price, desc, typeId } = req.body;
  try {
    const food = await foodServices.createFood({
      name,
      image,
      price,
      desc,
      typeId,
    });
    resp({
      res,
      data: food,
    });
  } catch (error) {
    next(error);
  }
};
