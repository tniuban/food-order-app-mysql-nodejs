const resp = require('../../helpers/response');
const foodServices = require('../../services/food');

module.exports = async (req, res, next) => {
	const args = req.body;
	try {
		const food = await foodServices.getFoods(args);
		resp({
			res,
			data: food,
		});
	} catch (error) {
		next(error);
	}
};
