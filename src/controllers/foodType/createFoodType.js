const resp = require('../../helpers/response');
const foodTypeServices = require('../../services/foodType');

module.exports = async (req, res, next) => {
  const { name } = req.body;
  try {
    const foodType = await foodTypeServices.createFoodType({ name });
    resp({
      res,
      data: foodType,
    });
  } catch (error) {
    next(error);
  }
};
