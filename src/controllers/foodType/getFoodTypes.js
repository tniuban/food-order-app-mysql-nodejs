const resp = require('../../helpers/response');
const foodTypeServices = require('../../services/foodType');

module.exports = async (req, res, next) => {
  const { limit, offset } = req.body;
  try {
    const foodType = await foodTypeServices.getFoodTypes({ limit, offset });
    resp({
      res,
      data: foodType,
    });
  } catch (error) {
    next(error);
  }
};
