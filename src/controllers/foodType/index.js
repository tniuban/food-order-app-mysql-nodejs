const foodTypeControllers = {
	createFoodType: require('./createFoodType'),
	getFoodTypes: require('./getFoodTypes'),
	findFoodTypeById: require('./findFoodTypeById'),
	updateFoodType: require('./updateFoodType'),
	deleteFoodType: require('./deleteFoodType'),
};

module.exports = foodTypeControllers;
