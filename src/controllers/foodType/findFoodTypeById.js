const resp = require('../../helpers/response');
const foodTypeServices = require('../../services/foodType');

module.exports = async (req, res, next) => {
	const { id } = req.body;
	try {
		const foodType = await foodTypeServices.findFoodTypeById(id);
		resp({
			res,
			data: foodType,
		});
	} catch (error) {
		next(error);
	}
};
