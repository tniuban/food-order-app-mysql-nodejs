const resp = require('../../helpers/response');
const orderServices = require('../../services/order');

module.exports = async (req, res, next) => {
  const { limit, offset } = req.body;
  try {
    const order = await orderServices.getOrders({ limit, offset });
    resp({
      res,
      data: order,
    });
  } catch (error) {
    next(error);
  }
};
