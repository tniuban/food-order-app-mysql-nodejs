const resp = require('../../helpers/response');
const orderServices = require('../../services/order');

module.exports = async (req, res, next) => {
	const { id } = req.body;
	try {
		const order = await orderServices.deleteOrder(id);
		resp({
			res,
			data: order,
		});
	} catch (error) {
		next(error);
	}
};
