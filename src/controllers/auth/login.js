const config = require('../../config');
const resp = require('../../helpers/response');
const authServices = require('../../services/auth');
const jwt = require('jsonwebtoken');

module.exports = async (req, res, next) => {
  const { email, password } = req.body;
  try {
    const user = await authServices.login({ email, password });
    const restaurantIds = user.restaurants.map(({ id }) => id);
    const jwtData = { id: user.id, restaurantIds };
    const accessToken = jwt.sign(jwtData, config.jwtSecretKey, {
      expiresIn: 86400, // 24 hours
    });

    const refreshToken = jwt.sign(jwtData, config.jwtSecretKey, {
      expiresIn: 86400 * 30, // 1 month
    });

    resp({
      res,
      data: { profile: user, accessToken, refreshToken },
    });
  } catch (error) {
    next(error);
  }
};
