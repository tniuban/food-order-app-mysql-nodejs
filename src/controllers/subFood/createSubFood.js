const resp = require('../../helpers/response');
const subFoodServices = require('../../services/subFood');

module.exports = async (req, res, next) => {
  const { name, price, foodId } = req.body;
  try {
    const subFood = await subFoodServices.createSubFood({
      name,
      price,
      foodId,
    });
    resp({
      res,
      data: subFood,
    });
  } catch (error) {
    next(error);
  }
};
