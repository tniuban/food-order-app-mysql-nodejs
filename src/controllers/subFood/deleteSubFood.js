const resp = require('../../helpers/response');
const subFoodServices = require('../../services/subFood');

module.exports = async (req, res, next) => {
	const { id } = req.body;
	try {
		const subFood = await subFoodServices.deleteSubFood(id);
		resp({
			res,
			data: subFood,
		});
	} catch (error) {
		next(error);
	}
};
