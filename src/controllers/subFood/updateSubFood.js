const resp = require('../../helpers/response');
const subFoodServices = require('../../services/subFood');

module.exports = async (req, res, next) => {
	const args = req.body;
	try {
		const subFood = await subFoodServices.updateSubFood(args);
		resp({
			res,
			data: subFood,
		});
	} catch (error) {
		next(error);
	}
};
