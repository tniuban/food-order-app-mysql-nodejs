
const resp = require('../../helpers/response');
const restaurantServices = require('../../services/restaurant');

module.exports = async (req, res, next) => {
	const args = req.body;
	try {
		const restaurant = await restaurantServices.updateRestaurant(args);
		resp({
			res,
			data: restaurant,
		});
	} catch (error) {
		next(error);
	}
};
