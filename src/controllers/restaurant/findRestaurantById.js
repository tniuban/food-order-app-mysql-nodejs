
const resp = require('../../helpers/response');
const restaurantServices = require('../../services/restaurant');

module.exports = async (req, res, next) => {
	const { id } = req.body;
	try {
		const restaurant = await restaurantServices.findRestaurantById(id);
		resp({
			res,
			data: restaurant,
		});
	} catch (error) {
		next(error);
	}
};
