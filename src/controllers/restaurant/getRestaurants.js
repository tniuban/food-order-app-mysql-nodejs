const resp = require('../../helpers/response');
const restaurantServices = require('../../services/restaurant');

module.exports = async (req, res, next) => {
  const { limit, offset } = req.body;
  try {
    const restaurants = await restaurantServices.getRestaurants({
      limit,
      offset,
    });
    resp({
      res,
      data: restaurants,
    });
  } catch (error) {
    next(error);
  }
};
