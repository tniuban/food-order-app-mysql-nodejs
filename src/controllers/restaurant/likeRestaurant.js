const resp = require('../../helpers/response');
const restaurantServices = require('../../services/restaurant');

module.exports = async (req, res, next) => {
  const { userId } = req;
  const { restaurantId } = req.body;
  try {
    const response = await restaurantServices.likeRestaurant({
      userId,
      restaurantId,
    });
    resp({
      res,
      data: response,
    });
  } catch (error) {
    next(error);
  }
};
