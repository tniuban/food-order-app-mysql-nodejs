const resp = require('../../helpers/response');
const restaurantServices = require('../../services/restaurant');

module.exports = async (req, res, next) => {
  const { userId } = req;
  const { restaurantId, amount } = req.body;
  try {
    const response = await restaurantServices.rateRestaurant({
      userId,
      restaurantId,
      amount,
    });
    resp({
      res,
      data: response,
    });
  } catch (error) {
    next(error);
  }
};
