const resp = require('../../helpers/response');
const restaurantServices = require('../../services/restaurant');

module.exports = async (req, res, next) => {
  const { name, description } = req.body;
  const { userId: ownerId } = req;
  try {
    const restaurant = await restaurantServices.createRestaurant({
      name,
      description,
      ownerId,
    });
    resp({
      res,
      data: restaurant,
    });
  } catch (error) {
    next(error);
  }
};
