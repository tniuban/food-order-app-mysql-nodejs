const router = require('express').Router();

const authRouter = require('./auth');
const userRouter = require('./user');
const restaurantRouter = require('./restaurant');
const foodRouter = require('./food');
const foodTypeRouter = require('./foodType');
const orderRouter = require('./order');

router.use('/auth', authRouter);
router.use('/users', userRouter);
router.use('/restaurants', restaurantRouter);
router.use('/foods', foodRouter);
router.use('/food-types', foodTypeRouter);
router.use('/orders', orderRouter);

module.exports = router;
