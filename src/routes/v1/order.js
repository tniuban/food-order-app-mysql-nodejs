const orderController = require('../../controllers/order');

const router = require('express').Router();

router.post('/create-order', orderController.createOrder);
router.post('/get-orders', orderController.getOrders);
router.post('/find-order-by-id', orderController.findOrderById);
router.post('/update-order', orderController.updateOrder);
router.post('/delete-order', orderController.deleteOrder);

module.exports = router;
