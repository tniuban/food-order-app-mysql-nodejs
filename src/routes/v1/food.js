const foodController = require('../../controllers/food');

const verifyToken = require('../../middleware/verifyToken');

const router = require('express').Router();

router.post('/create-food', foodController.createFood);
router.post('/get-foods', foodController.getFoods);
router.post('/find-food-by-id', foodController.findFoodById);
router.post('/update-food', foodController.updateFood);
router.post('/delete-food', foodController.deleteFood);

module.exports = router;
