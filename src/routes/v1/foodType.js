const foodTypeController = require('../../controllers/foodType');

const verifyToken = require('../../middleware/verifyToken');

const router = require('express').Router();

router.post('/create-food-type', foodTypeController.createFoodType);
router.post('/get-food-types', foodTypeController.getFoodTypes);
router.post('/find-food-type-by-id', foodTypeController.findFoodTypeById);
router.post('/update-food-type', foodTypeController.updateFoodType);
router.post('/delete-food-type', foodTypeController.deleteFoodType);

module.exports = router;
