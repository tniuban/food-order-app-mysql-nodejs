const restaurantController = require('../../controllers/restaurant');

const verifyToken = require('../../middleware/verifyToken');

const router = require('express').Router();

router.post(
  '/create-restaurant',
  [verifyToken],
  restaurantController.createRestaurant
);
router.post('/get-restaurants', restaurantController.getRestaurants);
router.post('/find-restaurant-by-id', restaurantController.findRestaurantById);
router.post(
  '/update-restaurant',
  [verifyToken],
  restaurantController.updateRestaurant
);
router.post(
  '/delete-restaurant',
  [verifyToken],
  restaurantController.deleteRestaurant
);

router.post(
  '/like-restaurant',
  [verifyToken],
  restaurantController.likeRestaurant
);
router.post('/get-restaurant-likes', restaurantController.getRestaurantLikes);
router.post(
  '/rate-restaurant',
  [verifyToken],
  restaurantController.rateRestaurant
);

router.post('/get-restaurant-rates', restaurantController.getRestaurantRates);
module.exports = router;
