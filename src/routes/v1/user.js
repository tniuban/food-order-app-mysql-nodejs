const userController = require('../../controllers/user');

const router = require('express').Router();

router.post('/create-user', userController.createUser);
router.post('/get-users', userController.getUsers);
router.post('/find-user-by-id', userController.findUserById);
router.post('/update-user', userController.updateUser);
router.post('/delete-user', userController.deleteUser);

module.exports = router;
